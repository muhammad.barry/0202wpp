from django.urls import path

from . import views

app_name = 'barryhaikal'

urlpatterns = [
    path('', views.index, name='haikal'),
]